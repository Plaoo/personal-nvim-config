require('plug')
require('settings')
require('mapping')
require('plugins/nvim-tree')
require('plugins/lualine')
require('plugins/zig')
require('plugins/fzf')
require('plugins/treesitter')
require('plugins/bufferline')
require('plugins/coq')
require('plugins/lspconfig')
require('plugins/nvim-web-devicons-config')
require('plugins/telescopeconfig')
require('plugins/nvim-autopairs')
require('plugins/mason')
require('plugins/nvim-completition')
require('plugins/vimwiki')
require('telescope').setup{  defaults = { file_ignore_patterns = { "plugged", ".git", "__pycache__" }} }
--require('plugins/avante')
--require('plugins/deepseek')
-- Markdown

--dap_debugging

require("dapui").setup()
require("dap-go").setup()
require('dap-python').setup('~/.virtualenvs/debugpy/bin/python')
local dap, dapui = require("dap"), require("dapui")

dap.configurations.python = {
    {
        type = 'python', 
        request = 'launch',
        name = "Launch file",
        program = "${file}",
        justMyCode = false,
    }
}

dap.listeners.before.attach.dapui_config = function()
  dapui.open()
end
dap.listeners.before.launch.dapui_config = function()
  dapui.open()
end
dap.listeners.before.event_terminated.dapui_config = function()
  dapui.close()
end
dap.listeners.before.event_exited.dapui_config = function()
  dapui.close()
end

