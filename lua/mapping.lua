local map = vim.api.nvim_set_keymap
local default_opts = { noremap = true, silent = true }
local cmd = vim.cmd
local g = vim.g         				-- global variable
g.mapleader = " "             -- change leader to a space
--g.mapleader = "<Space>"
--g.maplocalleader = "<Space>"
map('n', '<C-b>',       ':Buffers<CR>', default_opts)
map('n', '<Leader>gb',  ':Git blame<CR>', default_opts)
map('n', '<leader>gg',  ':LazyGit<CR>', default_opts)
map('n', "<Leader>q'",  "ciw''<Esc>P", default_opts)
map('n', '<Leader>q"',  'ciw""<Esc>P', default_opts)
map('n', '<Leader>q(',  'ciw()<Esc>P', default_opts)
map('n', '<Leader>q{',  'ciw{}<Esc>P', default_opts)
map('n', '<Leader>q[',  'ciw[]<Esc>P', default_opts)
map('n', '<Leader>ii',  ':e ~/.config/nvim/ <CR>', default_opts)
map('n', '<Leader>bf',  ':Black<CR>', default_opts)
map('n', '<F4>',        ':GoRun %<CR>', default_opts)
map('n', '<F10>',       ':Nuake<CR>', default_opts)
map('n', '<C-l>',       ':bnext<CR>', default_opts)
map('n', '<C-h>',       ':bprev<CR>', default_opts)
map('n', '<C-d>',       ':bd<CR>', default_opts)
map('n', '<C-l>',       ':bnext<CR>', default_opts)
map('n', '<Leader>bh', ':BufferLineMovePrev<CR>', default_opts)
map('n', '<Leader>bl', ':BufferLineMoveNext<CR>', default_opts)
map('n', '<C-h>',       ':bprev<CR>', default_opts)
-- map('n', '<F5>',	':COQnow<CR>', default_opts)
map('n', 'q', '<Nop>', default_opts)
map('n', 'Q', '<Nop>', default_opts)
map('n', '<F2>',	':SymbolsOutline<CR>', default_opts)
map('n', '<Leader>nn',  ':NvimTreeToggle<CR>', default_opts)
--terminal
map('n', '<Leader>tt',  ':terminal<CR>', default_opts)
map('t', '<Esc>', '<C-\\><C-n>', default_opts)
--Go
map('n', '<F9>',  ':GoRun .<CR>', default_opts)
--Telescope
--map('n', '<Leader>pp',  ':Telescope file_browser path=%:p:h<CR>', default_opts)
map('n', '<Leader>pp',  ':NvimTreeFindFile<CR>', default_opts)
map('n', '<Leader>ll',  ':Telescope find_files hidden=true<CR>', default_opts)
map('n', '<Leader>ff',  ':Ag ', default_opts)
map('n', '<Leader>ee',  ':Telescope diagnostics<CR>', default_opts)
map('n', '<Leader>cd',  ':Telescope man_pages<CR>', default_opts)
map('n', '<Leader>nm',  ':Telescope file_browser<CR>', default_opts)
map('n', '<Leader>bb',  ':Telescope buffers<CR>', default_opts)
map('n', '<Leader>bm',  ':Telescope vim_bookmarks all<CR>', default_opts)
map('n', '<Leader>bc',  ':Telescope vim_bookmarks current_file<CR>', default_opts)
map("n", "<Leader>dt", ":DapToggleBreakpoint<CR>", default_opts)
map("n", "<Leader>dc", ":DapContinue<CR>", default_opts)
map("n", "<Leader>dx", ":DapTerminate<CR>", default_opts)
map("n", "<Leader>do", ":DapStepOver<CR>", default_opts)
map("n", "<Leader>do", ":DapStepOver<CR>", default_opts)
map("n", "<Leader>tr", ":TransToEN<CR>", default_opts)
map("n", "<Leader>ss", ":stopinsert | :Telescope lsp_document_symbols<CR>", default_opts)


