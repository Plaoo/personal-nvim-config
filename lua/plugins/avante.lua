require('avante').setup({
  sections = {
    left = { 'mode', 'filename' },
    right = { 'filetype', 'filesize', 'cursorpos', 'deepseek_status' }  
  },
  colors = {
    background = '#1e1e1e',
    foreground = '#ffffff',
    accent = '#ff5f87'
  },
  separator = '|',
  inactive_separator = '|'
})

local deepseek = require('deepseek')

local function update_deepseek_status()
  local response = deepseek.query("Stato attuale")
  vim.api.nvim_command('echo "' .. response.choices[1].message.content .. '"')
end

-- Aggiorna lo stato ogni 10 secondi
vim.fn.timer_start(10000, update_deepseek_status, {['repeat'] = -1})
