require('bufferline').setup({
  options = {
    numbers = "ordinal", -- Mostra i numeri dei buffer
    diagnostics = "nvim_lsp", -- Mostra gli errori di LSP
    separator_style = "slant",
  },
})
