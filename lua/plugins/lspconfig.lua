local nvim_lsp = require("lspconfig")
local capabilities = require("cmp_nvim_lsp").default_capabilities()

local on_attach = function(client, bufnr)
    local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
    local opts = { noremap = true, silent = true }

    buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
    buf_set_keymap('n', '<Leader>rn', '<Cmd>lua vim.lsp.buf.rename()<CR>', opts)
    buf_set_keymap('n', 'gr', '<Cmd>lua vim.lsp.buf.references()<CR>', opts)
end


require("mason-lspconfig").setup_handlers({
    function(server_name)
        nvim_lsp[server_name].setup({
            on_attach = on_attach,
            capabilities = capabilities,
        })
    end,
    ["pyright"] = function()
        nvim_lsp.pyright.setup({
            on_attach = on_attach,
            capabilities = capabilities,
            settings = {
                python = {
                    analysis = {
                        typeCheckingMode = "off", -- Disabilita il controllo dei tipi
                        reportOptionalMemberAccess = "none", -- Nessun warning su membri opzionali
                        reportGeneralTypeIssues = "none", -- Nessun warning generico
                    }
                }
            }
        })
    end,
    ["eslint"] = function()
        nvim_lsp.eslint.setup({
            on_attach = on_attach,
            capabilities = capabilities,
            settings = { format = { enable = false } },
        })
    end,
})

