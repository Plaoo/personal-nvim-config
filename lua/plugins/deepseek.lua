local deepseek = {}

local function query_deepseek(prompt)
  local api_key = "sk-5a2503786320496eb2d8a778b392e7ee"
  local url = "https://api.deepseek.com/v1/chat/completions"  -- Endpoint API di DeepSeek

  local request_body = vim.fn.json_encode({
    model = "deepseek-chat",
    messages = {
      { role = "user", content = prompt }
    }
  })

  local handle = io.popen(string.format(
    'curl -s -X POST %s -H "Content-Type: application/json" -H "Authorization: Bearer %s" -d \'%s\'',
    url, api_key, request_body
  ))

  local result = handle:read("*a")
  handle:close()

  return vim.fn.json_decode(result)
end

deepseek.query = query_deepseek

return deepseek
