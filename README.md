# Instructions:
```cd ~/.config/nvim```

```git clone -b lua https://codeberg.org/Plaoo/personal-nvim-config.git . ```

# From nvim inline console:
```:PlugInstall```

# From terminal

```cd plugged/telescope-fzf-native.nvim```

```make```

# Debug

## Go
Install Delve

## Python

Create 
```
mkdir .virtualenvs
cd .virtualenvs
python -m venv debugpy
debugpy/bin/python -m pip install debugpy
```
